import UnrarHelper
import os
import glob
import rarfile

def main():
    """unrars and then deletes rar files"""
    # top level folder
    starting_dir = '/Volumes/untitled/folder'
    counter = 0
    unrar_counter = 0
    error_dirs = []

    # loop through all sub-dirs starting from starting_dir
    for root, dirs, files in os.walk(starting_dir):
        print root

        # create a list of all the rar files
        try:
            rar_list = glob.glob(os.path.join(root, '*.rar'))
        except:
            print('error in ' + root)
            continue

        # if there are rar files, unrar and then delete those files
        if rar_list:

            # create object
            all_files = UnrarHelper.Unrar(files)

            for rar in all_files.rar_files():
                try:
                    r_file = rarfile.RarFile(os.path.join(root,rar))
                    #extract file in same folder as rar file
                    r_file.extractall(path=root)
                    unrar_counter += 1

                    # get list of all files that make up archive
                    files_unrared = r_file.volumelist()

                    # after unrared, delete files that made up the archive
                    for item in files_unrared:
                        if os.path.isfile(item):
                            os.remove(item)
                except:
                    # adds folders where the rar was broken
                    error_dirs.append(root)
                    continue
        counter += 1

    print('Total folders scanned: ' + str(counter))
    print('Total files unrared: ' + str(unrar_counter))

    # if there are errors, write to file in current dir
    if error_dirs:
        with open('error.txt', 'a') as error_log:
            error_log.write(str(error_dirs))

if __name__ == '__main__':
    main()
